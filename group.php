<?php
    session_start(); 
  ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Группы</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<?php require_once('app\header.php') ?>

            <div class="row">
                <div class="col-sm-12 groups">
                    
                        <div class="card-deck" -->
                            <div class="card">
                                <div class="card-body">
                                <img class="card-img-top" src="https://parusadetstva.ru/content/images/2018/10/DSC08947-2-2.jpg" alt="Card image cap">
                                <h5 class="card-title">Группа 1</h5>
                                <p class="card-text">Начальное изучение английского языка</p>
                                <a href="#" class="btn btn-primary">Записаться</a>
                                </div>
                            </div>
                            
                            <div class="card" >
                                <div class="card-body">
                                    <img class="card-img-top" src="https://parusadetstva.ru/content/images/2018/10/DSC08947-2-2.jpg" alt="Card image cap">
                                <h5 class="card-title">Группа 2</h5>
                                <p class="card-text">Начальное изучение английского языка</p>
                                <a href="#" class="btn btn-primary">Записаться</a>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <img class="card-img-top" src="https://parusadetstva.ru/content/images/2018/10/DSC08947-2-2.jpg" alt="Card image cap">
                                <h5 class="card-title">Группа 3</h5>
                                <p class="card-text">Начальное изучение английского языка</p>
                                <a href="#" class="btn btn-primary">Записаться</a>
                                </div>
                            </div>
                         </div>
                    
                </div>
            </div>
        </div>   

    </header>

    <footer class="footer">
        <div class="container-fluid">
           <div class="row">
               
                   <div class="footer__text">Детский сад №201 "Волшебный замок"</div>
               
           </div>
        </div>
    </footer>
</body>
</html>